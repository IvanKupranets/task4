using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace Epam_Task4
{
    class Program
    {
        class Point
        {
            public int X { get; set;}
            public int Y { get; set;}
            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }
        }
        class Rectangle : IEnumerable
        {
            //Array of Points to hold points of Rectangle;
            private Point[] points = new Point[4];
            // Indexer
            public Point this[int i]
            {
                get { return points[i]; }
            }
            //Parameterized Constructor 
            public Rectangle(Point p1, Point p2, Point p3, Point p4)
            {
                points[0] = p1;
                points[1] = p2;
                points[2] = p3;
                points[3] = p4;
            }
            //Default Constructor
            public Rectangle() { }
            public void MoveUpOrDown(int step)
            {
                foreach (var i in points)
                {
                    i.Y += step;
                }
            }
            public void MoveLeftOrRight(int step)
            {
                foreach (var i in points)
                {
                    i.X += step;
                }
            }
            public Rectangle ChangeSizeBigger(int size)
            {
                points[0].X -= size;
                points[0].Y -= size;
                points[1].X -= size;
                points[1].Y += size;
                points[2].X += size;
                points[2].Y += size;
                points[3].X += size;
                points[3].Y -= size;
                return new Rectangle(new Point(points[0].X, points[0].Y), new Point(points[1].X, points[1].Y),
                    new Point(points[2].X, points[2].Y), new Point(points[3].X, points[3].Y));
            }
            public void ChangeSizeSmaller(int size)
            {
                points[0].X += size;
                points[0].Y += size;
                points[1].X += size;
                points[1].Y -= size;
                points[2].X -= size;
                points[2].Y -= size;
                points[3].X -= size;
                points[3].Y += size;
            }
            public IEnumerator GetEnumerator()
            {
                yield return points;
            }
            public void ShowCoordinates()
            {
                Console.WriteLine("Cordinates of rectangle");
                foreach (var i in this.points)
                {
                    Console.Write("({0},{1})", i.X, i.Y);
                }
                Console.WriteLine();
            }
            public Rectangle RectangleBigerThenTwo(Rectangle rec1, Rectangle  rec2)
            {
                int maxX = rec1.points[3].X;
                int maxY = rec1.points[1].Y;
                int minX = rec1.points[0].X;
                int minY = rec1.points[0].Y;
                int[] massX = new int[8];
                int[] massY = new int[8];
                for (int i = 0; i < points.Length; i++)
                {
                    massX[i] = rec1.points[i].X;
                    massY[i] = rec1.points[i].Y;   
                }
                int j = 4;
                for (int i = 0; i < points.Length; i++)
                {
                    massX[j] = rec2.points[i].X;
                    massY[j] = rec2.points[i].Y;
                    j++;
                }
                for (int i = 0; i < massX.Length; i++)
                {
                    if (massX[i] >= maxX)
                    {
                        maxX = massX[i];
                    }
                    if (massY[i] >= maxY)
                    {
                        maxY = massY[i];
                    }
                    if (massX[i] <= minX)
                    {
                        minX = massX[i];
                    }
                    if(massY[i] <= minY)
                    {
                        minY = massY[i];
                    }
                }
                Rectangle newRectangle = new Rectangle
                (new Point(minX, minY), new Point(minX, maxY), new Point(maxX, maxY), new Point(maxX, minY));
                return newRectangle.ChangeSizeBigger(1);
            }

        }
        
        static void Main(string[] args)
        {
            Rectangle rec = new Rectangle(new Point(1, 1), new Point(1, 3), new Point(3, 3), new Point(3, 1));
            rec.ShowCoordinates();
            rec.ChangeSizeBigger(1);
            rec.ShowCoordinates();
            rec.ChangeSizeSmaller(2);
            rec.ShowCoordinates();
            Rectangle rec1 = new Rectangle(new Point(1, 1), new Point(1, 3), new Point(3, 3), new Point(3, 1));
            Rectangle rec2 = new Rectangle(new Point(2, 1), new Point(2, 3), new Point(4, 3), new Point(4, 1));
            rec2.ShowCoordinates();
            Rectangle rec3 = new Rectangle();
            rec3 = rec3.RectangleBigerThenTwo(rec,rec2);
            rec3.ShowCoordinates();
            Console.ReadLine();
        }
    }
}
